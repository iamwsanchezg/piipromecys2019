package gui;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import core.Math;
import core.OnlyDigits;

public class FormMath extends Frame {
	/**
	 * 
	 */
	public FormMath() {
		initComponents();
	}
	private static final long serialVersionUID = 1L;
	static Desktop dp = Desktop.getDesktop();
	//Declaraci�n de componentes
	Panel p1;
	Panel p2;
	Label l1;
	TextField t1;
	Label l2;
	Button b1;
	Button b2;
	Button b3;
	private void initComponents() {
		setLayout(null);
		p1 = new Panel();
		p1.setLayout(null);
		p1.setBounds(20,40,350,100);
		p1.setBackground(Color.BLUE);				
		
		l1 = new Label("N�mero");
		l1.setBounds(20, 20, 100, 32);
		p1.add(l1);
		
		t1 = new TextField();
		t1.setBounds(125, 20, 100, 32);
		p1.add(t1);
		
		l2 = new Label("");	
		l2.setBounds(20, 60, 310, 32);
		l2.setForeground(Color.WHITE);
		l2.setAlignment(Label.CENTER);
		l2.setFont(new java.awt.Font("Dialog", 1, 16));
		p1.add(l2);
		
		p2 = new Panel();
		p2.setLayout(null);
		p2.setBounds(20,150,350,75);
		p2.setBackground(new Color(102,255,178));
		
		b1 = new Button("n Fibonacci");
		p2.add(b1);
		b1.setBounds(20, 20, 100, 32);
		
		b2 = new Button("n!");
		p2.add(b2);
		b2.setBounds(110, 20, 100, 32);
		
		b3 = new Button("�Es primo?");
		p2.add(b3);
		b3.setBounds(210, 20, 100, 32);
		
		add(p1);
		add(p2);
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
			}
		});	
		b1.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if(Math.isNumeric(t1.getText())) {
					int n = Integer.parseInt(t1.getText());
					double x = Math.getFibonacci(n);
					l2.setText(String.valueOf(x));
				}
			}
		});
		b2.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if(Math.isNumeric(t1.getText())) {
					int n = Integer.parseInt(t1.getText());
					double x = Math.getFactorial(n);
					l2.setText(String.valueOf(x));
				}
			}
		});
		b3.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if(Math.isNumeric(t1.getText())) {
					int n = Integer.parseInt(t1.getText());
					boolean x = Math.isPrimeNumber(n);
					l2.setText(String.valueOf(x?"Es Primo":"No es primo"));
				}
			}
		});
		t1.addKeyListener(new OnlyDigits());
		setBounds(0,0,390,250);
		//pack();
	}
}
