package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class FormMain extends Frame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	MenuBar mainMenu;
	Menu fileMenu;
	Menu operationMenu;
	MenuItem openMenuItem;
	MenuItem optionMenuItem;
	
	FileDialog fg;
	Label fileNameLabel;
	Panel pageStartPanel;
	Panel centerPanel;
	Panel lineStartPanel;
	Panel pageEndPanel;
	Panel lineEndPanel;
	
	public FormMain() {
		initComponents();
	}
	private void initComponents() {
		setLayout(new BorderLayout());
		pageStartPanel = new Panel();
		centerPanel = new Panel();
		lineStartPanel = new Panel();
		lineEndPanel = new Panel();
		pageEndPanel = new Panel();
		
		fileNameLabel = new Label();
		fileNameLabel.setSize(500, 100);
		
		pageStartPanel.setLayout(new BorderLayout());
		pageStartPanel.add(fileNameLabel,BorderLayout.CENTER);
		pageStartPanel.setBackground(Color.BLUE);		
		add(pageStartPanel, BorderLayout.PAGE_START);
		
		centerPanel.setBackground(Color.WHITE);
		add(centerPanel, BorderLayout.CENTER);
		
		lineStartPanel.setBackground(Color.YELLOW);		
		add(lineStartPanel, BorderLayout.LINE_START);
		
		lineEndPanel.setBackground(Color.YELLOW);
		add(lineEndPanel, BorderLayout.LINE_END);
		
		pageEndPanel.setBackground(Color.BLUE);
		add(pageEndPanel, BorderLayout.PAGE_END);
		

		mainMenu = new MenuBar();
		
		fileMenu = new Menu("Archivo");
		openMenuItem = new MenuItem("Abrir");
		fileMenu.add(openMenuItem);
		
		operationMenu = new Menu("Operaciones");
		optionMenuItem = new MenuItem("Operaciones especiales");
		operationMenu.add(optionMenuItem);
		
		openMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	ShowOpenFileDialog();
            }
        });
		
		operationMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	ShowFormMath();
            }
        });
		
		mainMenu.add(fileMenu);
		mainMenu.add(operationMenu);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setMenuBar(mainMenu);
		
		pack();
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});	
	}
	private void ShowOpenFileDialog() {
    	fg = new FileDialog(this, "Abrir un archivo");
    	fg.setVisible(true);
    	String file = fg.getDirectory() + fg.getFile();
    	fileNameLabel.setText("Archivo a abrir - " + file); 
	}
	private void ShowFormMath() {
		FormMath form = new FormMath();
		form.setVisible(true);		
	}	
}
