package gui;
import java.awt.*;
import java.awt.event.*;

public class Sum {
	// declaracion, creacion e inicializacion de componentes, objetos y variables
	static Frame SumForm = new Frame();
	static Label l1 = new Label("Ingrese valor1:");
	static TextField t1 = new TextField(3);
	static Label l2 = new Label("Ingrese valor2:");
	static Label l3 = new Label("Resultado:");
	static TextField t2 = new TextField(3);
	static TextField t3 = new TextField(5);
	static Button b1 = new Button("sumar");

	// parte principal de programa
	public static void main(String[] args) {
		// area de definicion de propiedades de el objeto
		SumForm.setTitle("Programa Sumatoria");
		SumForm.setLayout(new FlowLayout());
		SumForm.add(l1);
		SumForm.add(t1);
		SumForm.add(l2);
		SumForm.add(t2);
		SumForm.add(l3);
		SumForm.add(t3);
		SumForm.add(b1);
		SumForm.pack();
		SumForm.setVisible(true);

		// area de asociacion de objeto-eventos
		b1.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				int a = Integer.parseInt(t1.getText());
				int b = Integer.parseInt(t2.getText());
				int c = a + b;
				t3.setText(String.valueOf(c));
			}
		});
		SumForm.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}; // termina main
} // termina clase
