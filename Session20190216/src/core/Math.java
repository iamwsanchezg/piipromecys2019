package core;

public class Math {
	//Devuelve el n t�rmino de la serie Fibonacci
	//La sucesi�n comienza con los n�meros 0 y 1, y a partir de estos, �cada t�rmino es la suma de los dos anteriores�
	public static double getFibonacci(int n) {
	    double s=1,u=1,p=1;
	    int i;
	    if(n==0) {
	    	s=0;
	    }
	    else {
		    for(i=3;i<=n;i++)
		    {
		        p=u;
		        u=s;
		        s=s+p;
		    }	    	
	    }
	    return s;
	}
	
	//Devuel el factorial de un entero positivo
	//el factorial de n o n factorial se define en principio como el producto de todos los n�meros enteros positivos desde 1 hasta n 
    public static double getFactorial(int n){
        int i;
        double f=1;
        if (n>1)
        for (i=2;i<=n;i++)
            f*=i;
        return f;
    }
    
    //En matem�ticas, un n�mero primo es un n�mero natural mayor que 1 que tiene �nicamente dos divisores distintos: �l mismo y el 1.
    public static boolean isPrimeNumber(int n) {
        int C=0,i=1;
        if (n>2)
          while ((i<=n)&&(C<3)){
               if((n%i)==0)
                   C++;
               i++;
           }
       if (C>2)
           return false;
       else
           return true;
    }
    
    public static boolean isNumeric(String s) {  
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");  
    }  
}
